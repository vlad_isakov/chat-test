<?php

namespace app\models;

use app\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;

/**
 * Поля таблицы:
 *
 * @property integer $id
 * @property integer $text
 * @property bool $is_deleted
 * @property bool $is_admin
 * @property string $user_name
 * @property integer $user_id
 * @property string $insert_stamp
 * @property string $update_stamp
 *
 * @property-read User $user
 */
class ChatMessage extends ActiveRecord
{
    const ATTR_ID = 'id';
    const ATTR_TEXT = 'text';
    const ATTR_IS_DELETED = 'is_deleted';
    const ATTR_IS_ADMIN = 'is_admin';
    const ATTR_USER_NAME = 'user_name';
    const ATTR_USER_ID = 'user_id';
    const ATTR_INSERT_STAMP = 'insert_stamp';
    const ATTR_UPDATE_STAMP = 'update_stamp';

    const REL_USER = 'user';

    /**
     * @inheritDoc
     * @author Исаков Владислав
     */
    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => static::ATTR_INSERT_STAMP,
                'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
                'value'              => new Expression('now()'),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules() {
        return [
            [static::ATTR_TEXT, RequiredValidator::class],
            [static::ATTR_TEXT, StringValidator::class, 'max' => 255],
        ];
    }

    /**
     * @return array
     * @author Исаков Владислав
     */
    public function attributeLabels() {
        return [
            static::ATTR_ID           => 'id',
            static::ATTR_USER_NAME    => 'Пользователь',
            static::ATTR_IS_DELETED   => 'Удален',
            static::ATTR_TEXT         => 'Сообщение',
            static::ATTR_IS_ADMIN     => 'Сообщение администратора',
            static::ATTR_INSERT_STAMP => 'insert_stamp',
            static::ATTR_UPDATE_STAMP => 'update_stamp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @author Исаков Владислав
     */
    public function getUser() {
        return $this->hasOne(User::class, [User::ATTR_ID => static::ATTR_USER_ID]);
    }
}