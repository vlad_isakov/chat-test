<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Поля таблицы:
 *
 * @property string $name
 *
 */
class Role extends ActiveRecord
{
    const ATTR_NAME = 'name';

    public static function tableName() {
        return 'auth_item';
    }
}