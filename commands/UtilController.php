<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;
use Yii;

class UtilController extends Controller
{
    /**
     * Добавление тестовых пользователей в базу
     *
     * @return int
     * @author Исаков Владислав
     */
    public function actionAddUsers() {
        $auth = Yii::$app->authManager;

        // добавляем роль "user"
        $user = $auth->createRole('user');
        $auth->add($user);

        // добавляем роль "admin"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $user);

        $users = [
            'admin',
            'user',
            'user1',
            'user2',
            'user3',
        ];

        foreach ($users as $userName) {
            $model = User::find()->where(['username' => $userName])->one();
            if (empty($model)) {
                $user = new User();
                $user->username = $userName;
                $user->email = $userName . '@chat.loc';
                $user->setPassword($userName);
                $user->generateAuthKey();
                if ($user->save()) {
                    $auth = Yii::$app->authManager;

                    if ($userName === 'admin') {
                        $role = $auth->getRole('admin');
                    }
                    else {
                        $role = $auth->getRole('user');
                    }

                    $auth->assign($role, $user->id);

                    echo $user->username . ' добавлен.' . PHP_EOL;
                }
            }
        }

        return ExitCode::OK;
    }

}
