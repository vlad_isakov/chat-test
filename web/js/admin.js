(function ($) {
    'use strict';
    $.fn.adminPlugin = function () {
        const CHANGE_URL = $('.change-url').data('url');
        const methods = {
            init: function () {
                $('.change-message').click(function () {
                    $.ajax({
                        url: CHANGE_URL + '?id=' + $(this).data('id'),
                        method: "POST",
                        async: true
                    }).done(function (data) {
                        $.pjax.reload({container: '#chat', async: false});
                    });
                });
            }
        };
        methods.init();
    };
})(jQuery);