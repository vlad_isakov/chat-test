<?php
namespace app\controllers;

use yii\helpers\StringHelper;
use Yii;
use yii\web\Controller;

/**
 * Class BaseController
 *
 * @package backend\controllers
 *
 * @author Исаков Владислав
 */
class BaseController extends Controller {
    /**
     * Получение ссылки на указанное действие исходя из контроллера.
     *
     * @param string $actionName   Название действия
     * @param array  $actionParams Дополнительные параметры
     *
     *
     * @return string
     * @author Isakov Vladislav
     *
     */
    public static function getActionUrl($actionName, array $actionParams = []) {
        $prefix = null;

        $controllerName = preg_replace('/Controller$/', '', StringHelper::basename(static::class));
        $controllerName = mb_strtolower(preg_replace('~(?!\b)([A-Z])~', '-\\1', $controllerName));

        $actionParams[0] = implode('/', [
            $controllerName,
            $actionName,
        ]);

        $actionParams[0] = '/' . $prefix . $actionParams[0];

        /** @var string $url */
        $url = Yii::$app->urlManager->createUrl($actionParams);

        return $url;
    }
}