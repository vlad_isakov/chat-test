<?php

namespace app\controllers;

use app\models\Role;
use app\models\User;
use yii\filters\AccessControl;
use app\models\ChatMessage;
use Yii;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class UserController extends BaseController
{
    const ACTION_INDEX = 'index';
    const ACTION_UPDATE = 'update';

    /**
     * @inheritDoc
     * @author Исаков Владислав
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => [static::ACTION_INDEX, static::ACTION_UPDATE],
                'rules' => [
                    [
                        'actions' => [static::ACTION_INDEX],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                    [
                        'actions' => [static::ACTION_UPDATE],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    static::ACTION_INDEX  => ['get'],
                    static::ACTION_UPDATE => ['get', 'post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $users = User::find()->all();

        return $this->render('index', ['users' => $users]);
    }

    /**
     * @param int $id
     * @return string
     * @throws HttpException
     * @author Исаков Владислав
     */
    public function actionUpdate($id) {
        $user = User::findOne($id);

        if (null === $user) {
            throw new HttpException('Пользователь не найден');
        }

        if (Yii::$app->request->isPost) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole(Yii::$app->request->post('User')['role']);

            if (null === $role) {
                throw new HttpException('Роль не найдена');
            }

            $auth->revokeAll($user->id);
            $auth->assign($role, $user->id);

            return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
        }

        $roles = Role::find()->all();

        return $this->render('update', ['user' => $user, 'roles' => $roles]);
    }
}