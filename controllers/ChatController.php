<?php

namespace app\controllers;

use app\models\User;
use yii\filters\AccessControl;
use app\models\ChatMessage;
use Yii;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class ChatController extends BaseController
{
    const ACTION_INDEX = 'index';
    const ACTION_CHANGE_MESSAGE = 'change-message';

    /**
     * @inheritDoc
     * @author Исаков Владислав
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => [static::ACTION_CHANGE_MESSAGE],
                'rules' => [
                    [
                        'actions' => [static::ACTION_CHANGE_MESSAGE],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    static::ACTION_INDEX          => ['get', 'post'],
                    static::ACTION_CHANGE_MESSAGE => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param bool $deleted
     *
     * @return string
     *
     * @author Исаков Владислав
     */
    public function actionIndex($deleted = 0) {

        if (Yii::$app->request->isPost && false === Yii::$app->user->isGuest) {
            $chatMessage = new ChatMessage();
            $user = User::findOne(Yii::$app->user->id);

            $chatMessage->load(Yii::$app->request->post());
            $chatMessage->user_id = Yii::$app->user->id;
            $chatMessage->user_name = $user->username;
            $chatMessage->is_admin = Yii::$app->user->can('admin');
            $chatMessage->save();
        }

        $query = ChatMessage::find();
        $isAdmin = Yii::$app->user->can('admin');
        if (false === $isAdmin) {
            $query->andWhere([ChatMessage::ATTR_IS_DELETED => false]);
        }
        else {
            if (1 == $deleted) {
                $query->andWhere([ChatMessage::ATTR_IS_DELETED => true]);
            }
        }

        $messages = $query
            ->orderBy([ChatMessage::ATTR_INSERT_STAMP => SORT_DESC])
            ->limit(100)
            ->all();

        $model = new ChatMessage();

        return $this->render('index', ['messages' => $messages, 'isAdmin' => $isAdmin, 'model' => $model]);
    }

    /**
     * @param int $id
     *
     * @throws HttpException
     *
     * @author Исаков Владислав
     */
    public function actionChangeMessage($id) {
        $chatMessage = ChatMessage::findOne($id);

        if (null === $chatMessage) {
            throw new HttpException('Собощение не найдено');
        }

        $chatMessage->is_deleted = !$chatMessage->is_deleted;
        $chatMessage->save();
    }
}