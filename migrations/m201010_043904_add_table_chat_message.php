<?php

use yii\db\Migration;
use yii\db\mssql\Schema;

/**
 * Class m201010_043904_add_table_chat_message
 */
class m201010_043904_add_table_chat_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('chat_message', [
            'id'           => Schema::TYPE_PK,
            'text'         => $this->string(255)->notNull(),
            'is_deleted'   => $this->boolean()->defaultValue(false),
            'is_admin'     => $this->boolean()->defaultValue(false),
            'user_name'    => $this->string(255)->notNull(),
            'user_id'      => $this->integer()->notNull(),
            'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
            'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
        ]);

        $this->createIndex('ix-is_deleted', 'chat_message', 'is_deleted');
        $this->createIndex('ix-user_id', 'chat_message', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('chat_message');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_043904_add_table_chat_message cannot be reverted.\n";

        return false;
    }
    */
}
