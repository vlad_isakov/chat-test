<?php
use app\controllers\ChatController;

/* @var $this yii\web\View */

$this->title = 'Тестовое задание - Чат';
?>
<div class="site-index">
    <div class="jumbotron">
        <p><a class="btn btn-lg btn-success" href="<?= ChatController::getActionUrl(ChatController::ACTION_INDEX)?>">Чат</a></p>
    </div>
</div>
