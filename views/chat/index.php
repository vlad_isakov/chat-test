<?php

use app\controllers\ChatController;
use yii\widgets\Pjax;
use app\models\ChatMessage;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @author Исаков Владислав
 * @var ChatMessage[] $messages
 * @var ChatMessage $model
 * @var bool $isAdmin
 */

?>
<?php Pjax::begin(['id' => 'chat']); ?>
<?php if ($isAdmin): ?>
    <div class="change-url"
         data-url="<?= ChatController::getActionUrl(ChatController::ACTION_CHANGE_MESSAGE) ?>">
    </div>
    <?php $this->registerJs('$(".change-url").adminPlugin();'); ?>
<?php endif ?>
<?php if (false === Yii::$app->user->isGuest): ?>
    <?php $htmlForm = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <div class="row add-message">
        <div class="col-md-12">
            <?= $htmlForm->field($model, $model::ATTR_TEXT)->textarea() ?>
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
<?php endif ?>
<hr>
<?php foreach ($messages as $message): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="message <?= ($message->is_deleted ? 'alert alert-danger' : 'alert alert-info') ?> <?= ($message->is_admin ? 'admin' : '') ?>">
                <div class="author"><?= $message->user_name ?></div>
                <div class="text"><?= htmlspecialchars($message->text) ?></div>
                <div class="date"><?= $message->insert_stamp ?></div>
                <?php if ($isAdmin): ?>
                    <div class="btn <?=($message->is_deleted ? 'btn-info' : 'btn-danger')?> change-message"
                         data-id="<?= $message->id ?>"><?= ($message->is_deleted ? 'Восстановить' : 'Удалить') ?></div>
                <?php endif ?>
            </div>
        </div>
    </div>
<hr>
<?php endforeach ?>
<?php Pjax::end() ?>

