<?php
use app\models\User;
use app\models\Role;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;

/**
 * @author Исаков Владислав
 * @var User $user
 * @var Role[] $roles
 */

$htmlForm = ActiveForm::begin();

?>
<div class="row">
    <div class="col-md-4">
        <?= $htmlForm->field($user, $user::ATTR_ROLE)->dropDownList(ArrayHelper::map($roles, 'name', 'name')) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>