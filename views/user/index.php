<?php

use app\models\User;
use app\controllers\UserController;

/**
 * @author Исаков Владислав
 * @var User[] $users
 */

?>
<table class="table table-bordered">
    <tr>
        <th>Имя</th>
        <th>Роль</th>
        <th width="150px">#</th>
    </tr>
    <?php foreach ($users as $user): ?>
        <?php $userRoles = Yii::$app->authManager->getRolesByUser($user->id) ?>
        <tr>
            <td><?= $user->username ?></td>
            <td>
                <?php foreach ($userRoles as $userRole): ?>
                    <p><?= $userRole->name ?></p>
                <?php endforeach ?>
            </td>
            <td class="text-center"><a class="btn btn-info"
                                       href="<?= UserController::getActionUrl(UserController::ACTION_UPDATE, ['id' => $user->id]) ?>">Изменить</a>
            </td>
        </tr>
    <?php endforeach ?>
</table>
